FROM python
COPY . /opt/helloflask
RUN pip install -r /opt/helloflask/requirements.txt
EXPOSE 80
CMD /opt/helloflask/app.py
