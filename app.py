#!/usr/bin/env python3
from flask import Flask
from flask import render_template
import platform

app= Flask(__name__)

@app.route("/")
def index():
    #return "Hello from Flask"
    node = platform.node()
    return render_template("index.html", title="Welcome", name="Everybody", hostname=node)

app.run(host='0.0.0.0',port=80)



